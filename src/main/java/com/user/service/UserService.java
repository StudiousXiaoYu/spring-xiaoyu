package com.user.service;


import com.user.config.*;

@Component
public class UserService implements ApplicationContextAware, BeanNameAware {

    @AutoWired
    ServiceDemo serviceDemo;

    private XiaoyuApplicationContext applicationContext;
    private String beanName;

    public void test() {
        serviceDemo.say();
//        System.out.println(serviceDemo);
        System.out.println("userService:"+applicationContext.getBean("userService"));
        System.out.println("beanName:"+beanName);
    }

    @Override
    public void setApplicationContext(XiaoyuApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }

    @Override
    public void setBeanName(String beanName) {
        this.beanName = beanName;
    }
}
