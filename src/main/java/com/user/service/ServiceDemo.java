package com.user.service;

import com.user.config.Component;
import com.user.config.Transaction;

/**
 * <p>类的作用说明</p>
 *
 * @version 1.0
 * @since 2023/04/11 10:52:43
 */
@Component
@Transaction
public class ServiceDemo {

    public void say(){
        System.out.println("hello");
    }
}
