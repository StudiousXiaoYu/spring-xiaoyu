package com.user.config;

public interface BeanNameAware {

    void setBeanName(String beanName);
}
