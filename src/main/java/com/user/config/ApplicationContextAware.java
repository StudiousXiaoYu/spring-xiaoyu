package com.user.config;

public interface ApplicationContextAware {

    void setApplicationContext(XiaoyuApplicationContext applicationContext);
}
