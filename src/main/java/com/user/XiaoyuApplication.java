package com.user;

import com.user.config.MyDemoConfig;
import com.user.config.XiaoyuApplicationContext;
import com.user.service.UserService;

/**
 * <p>模拟Spring启动类</p>
 *
 * @version 1.0
 * @since 2023/04/10 18:00:08
 */
public class XiaoyuApplication {

    public static void main(String[] args) {
        XiaoyuApplicationContext xiaoyuApplicationContext = new XiaoyuApplicationContext(MyDemoConfig.class);
        UserService userService = (UserService) xiaoyuApplicationContext.getBean("userService");
        userService.test();
        userService.test();
    }

}
